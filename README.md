# Base Docker Images for Ubuntu and Alpine

## Build

### Ubuntu 18.04

```bash
  docker build -f Dockerfile.ubuntu_18_04 .
```

### Ubuntu 17.10

```bash
  docker build -f Dockerfile.ubuntu_17_10 .
 ```
  

### Alpine 3.7

```bash
  docker build -d Dockerfile.alpine_3_7 .
```

